﻿#include <iostream>
#include <iomanip>
#include <ctime>

int main()
{
    setlocale(LC_ALL, "ru");
    const int N = 5;
    int mas[N][N];
    for (int i = 0; i < N; i++)
        for (int j = 0; j < N; j++)
            mas[i][j] = i + j;

    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
            std::cout << mas[i][j]<<" ";
        std::cout << "\n";
    }

    struct tm newtime;
    __time64_t long_time;
    errno_t err;
    _time64(&long_time);
    err = _localtime64_s(&newtime, &long_time);
    if (err)
        return 1;

    int sum = 0;

    for (int j = 0; j < N; j++)
        sum += mas[newtime.tm_mday % N][j];

    std::cout<<"сумма равна: " << sum;
}

